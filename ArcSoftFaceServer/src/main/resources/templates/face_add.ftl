<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>人脸检测</title>
    <#import "common/common.macro.ftl" as netCommon>
    <@netCommon.commonStyle />
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <@netCommon.commonSidebar "人脸注册" />
    <div class="content-wrapper" align="center">
        <h4 align="center" style="margin-top: 30px">
            人脸注册示例
        </h4>
        <div>
            <label>姓名：</label>
            <input class="table-danger"
                   placeholder="在此输入姓名" type="text" name="userName" id="userName">
        </div>

        <div id="mainDiv">

        </div>
        <div>
            <table frame="void">
                <tr>
                    <td>
                        <button style="color:#FFFFFF;height: 30px;display:block;margin:0 auto;margin-top:10px;width:120px;background-color: #3F51B5;border-radius:5px;text-align: center;line-height: 30px;font-size: 20px"
                                onclick="imageRecog()">照片注册
                        </button>
                    </td>
                </tr>
                <#--<td><button id="snap" onclick="commitPhoto()" style="color:#FFFFFF;height: 30px;display:block;margin:0 auto;margin-top:10px;width:100px;background-color: #3F51B5;border-radius:5px;text-align: center;line-height: 30px;font-size: 20px">照片提交</button></td>-->
                <tr>
                    <td colspan="2">
                        <button id="snap" onclick="takePhoto()"
                                style="color:#FFFFFF;height: 30px;display:block;margin:0 auto;margin-top:10px;width:100px;background-color: #3F51B5;border-radius:5px;text-align: center;line-height: 30px;font-size: 20px">
                            提交
                        </button>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right">

        </div>
    </div>
</div>
<@netCommon.commonScript />
<script>
    //照片
    function takePhoto() {
        let mainComp = $("#mainDiv");

        var formData = new FormData();
        let userName = $("#userName").val();
        formData.append("groupId", "101");
        var file = $("#file0")[0].files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            var base64 = reader.result;
            formData.append("file", base64);
            formData.append("name", userName);
            $.ajax({
                type: "post",
                url: "/faceAdd",
                data: formData,
                contentType: false,
                processData: false,
                async: false,
                success: function (text) {
                    var res = JSON.stringify(text)
                    if (text.code == 0) {
                        alert("注册成功")
                    } else {
                        alert(text.message)
                    }
                },
                error: function (error) {
                    alert(JSON.stringify(error))
                }
            });
            location.reload();
        }
    }

    function imageRecog() {
        let imageInput = " <h2>点击图片区域上传文件</h2><input style='display: none' type='file'  name='file0' id='file0' multiple='multiple' /><br><img src='' id='img0' onclick='toChooseFile()' style='width: 30rem;height: 25rem;'>";
        $("#mainDiv").empty();
        $("#mainDiv").append(imageInput);
    }

    function toChooseFile() {
        $("#file0").trigger('click');
    }

    $(document).on("change", "#file0", function () {
        var objUrl = getObjectURL(this.files[0]);//获取文件信息
        console.log("objUrl = " + objUrl);
        if (objUrl) {
            $("#img0").attr("src", objUrl);
        }
    });

    function getObjectURL(file) {
        var url = null;
        if (window.createObjectURL != undefined) {
            url = window.createObjectURL(file);
        } else if (window.URL != undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file);
        } else if (window.webkitURL != undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file);
        }
        return url;
    }


</script>
<style>
    .content-wrapper {
        width: 800px;
        background-color: #ffffff;
    }

</style>

</body>
</html>