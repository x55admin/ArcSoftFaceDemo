package com.itboyst.facedemo.mapper;

import com.itboyst.facedemo.entity.UserFaceInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserFaceInfoMapper {

    List<UserFaceInfo> findUserFaceInfoList();

    void insertUserFaceInfo(UserFaceInfo userFaceInfo);

    List<UserFaceInfo> getUserFaceInfoByGroupId(Integer groupId);

}
