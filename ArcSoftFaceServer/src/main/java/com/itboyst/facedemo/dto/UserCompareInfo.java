package com.itboyst.facedemo.dto;


import com.itboyst.facedemo.entity.UserFaceInfo;

/**
 * @author st7251
 * @date 2020/3/11 14:02
 */
public class UserCompareInfo extends UserFaceInfo {
    private Float similar;

    public Float getSimilar() {
        return similar;
    }

    public void setSimilar(Float similar) {
        this.similar = similar;
    }
}
