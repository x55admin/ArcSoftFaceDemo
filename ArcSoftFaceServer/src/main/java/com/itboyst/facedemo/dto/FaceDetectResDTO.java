package com.itboyst.facedemo.dto;

import com.arcsoft.face.Rect;
import lombok.Data;

/**
 * @author st7251
 * @date 2020/3/13 14:50
 */
@Data
public class FaceDetectResDTO {
    /**
     * 姓名
     */
    private String name;
    /**
     * 年龄
     */
    private int age = -1;
    /**
     * 性别
     */
    private int gender = -1;
    /**
     * 是否时活体
     */
    private int liveness = -1;

    /**
     * 人脸位置信息
     */
    Rect rect;
    /**
     * 人脸角度信息
     */
    int orient;
    /**
     * 人脸id
     */
    int faceId = -1;
    /**
     * 戴眼镜的置信度[0-1],推荐阈值：0.5
     */
    float wearGlasses;
    /**
     * 左眼状态 -1:默认输出  0:未闭眼  1:闭眼
     */
    int leftEyeClosed;
    /**
     * 右眼状态 -1:默认输出  0:未闭眼  1:闭眼
     */
    int rightEyeClosed;
    /**
     * 人脸是否被遮挡 "1" 遮挡, "0" 未遮挡, "-1" 不确定
     */
    int faceShelter;
    /**
     * 人脸数据
     */
    byte[] faceData;
    /**
     * 人脸数据的长度
     */
    public static final int FACE_DATA_SIZE = 5000;
    /**
     * 底库位置
     */
    private String baseImgPath;
    private float similar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public int getOrient() {
        return orient;
    }

    public void setOrient(int orient) {
        this.orient = orient;
    }

    public int getFaceId() {
        return faceId;
    }

    public void setFaceId(int faceId) {
        this.faceId = faceId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getLiveness() {
        return liveness;
    }

    public void setLiveness(int liveness) {
        this.liveness = liveness;
    }

    public String getBaseImgPath() {
        return baseImgPath;
    }

    public void setBaseImgPath(String baseImgPath) {
        this.baseImgPath = baseImgPath;
    }

    public float getSimilar() {
        return similar;
    }

    public void setSimilar(float similar) {
        this.similar = similar;
    }

    public float getWearGlasses() {
        return wearGlasses;
    }

    public void setWearGlasses(float wearGlasses) {
        this.wearGlasses = wearGlasses;
    }

    public int getLeftEyeClosed() {
        return leftEyeClosed;
    }

    public void setLeftEyeClosed(int leftEyeClosed) {
        this.leftEyeClosed = leftEyeClosed;
    }

    public int getRightEyeClosed() {
        return rightEyeClosed;
    }

    public void setRightEyeClosed(int rightEyeClosed) {
        this.rightEyeClosed = rightEyeClosed;
    }

    public int getFaceShelter() {
        return faceShelter;
    }

    public void setFaceShelter(int faceShelter) {
        this.faceShelter = faceShelter;
    }

    public byte[] getFaceData() {
        return faceData;
    }

    public void setFaceData(byte[] faceData) {
        this.faceData = faceData;
    }
}
