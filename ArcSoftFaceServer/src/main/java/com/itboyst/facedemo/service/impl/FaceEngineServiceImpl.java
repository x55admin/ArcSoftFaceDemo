package com.itboyst.facedemo.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.arcsoft.face.*;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ExtractType;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.google.common.collect.Lists;
import com.itboyst.facedemo.dto.FaceDetectResDTO;
import com.itboyst.facedemo.dto.ProcessInfo;
import com.itboyst.facedemo.dto.UserCompareInfo;
import com.itboyst.facedemo.entity.UserFaceInfo;
import com.itboyst.facedemo.enums.ErrorCodeEnum;
import com.itboyst.facedemo.factory.FaceEngineFactory;
import com.itboyst.facedemo.mapper.UserFaceInfoMapper;
import com.itboyst.facedemo.rpc.BusinessException;
import com.itboyst.facedemo.service.FaceEngineService;
import com.itboyst.facedemo.util.IDUtil;
import com.itboyst.facedemo.util.ImageUtil;
import com.itboyst.facedemo.util.UserRamCache;
import com.itboyst.im.ServerLauncherImpl;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


@Service
public class FaceEngineServiceImpl implements FaceEngineService {

    public final static Logger logger = LoggerFactory.getLogger(FaceEngineServiceImpl.class);

    @Value("${config.arcface-sdk.sdk-lib-path}")
    public String sdkLibPath;
    @Value("${config.arcface-sdk.base-image-path}")
    public String baseImagePath;
    @Value("${config.arcface-sdk.app-id}")
    public String appId;

    @Value("${config.arcface-sdk.sdk-key}")
    public String sdkKey;

    @Value("${config.arcface-sdk.active-key}")
    public String activeKey;

    @Value("${config.arcface-sdk.detect-pool-size}")
    public Integer detectPooSize;

    @Value("${config.arcface-sdk.compare-pool-size}")
    public Integer comparePooSize;

    private ExecutorService compareExecutorService;

    //通用人脸识别引擎池
    private GenericObjectPool<FaceEngine> faceEngineGeneralPool;

    //人脸比对引擎池
    private GenericObjectPool<FaceEngine> faceEngineComparePool;

    @Resource
    private UserFaceInfoMapper userFaceInfoMapper;


    @PostConstruct
    public void init() {

        GenericObjectPoolConfig detectPoolConfig = new GenericObjectPoolConfig();
        detectPoolConfig.setMaxIdle(detectPooSize);
        detectPoolConfig.setMaxTotal(detectPooSize);
        detectPoolConfig.setMinIdle(detectPooSize);
        detectPoolConfig.setLifo(false);
        EngineConfiguration detectCfg = new EngineConfiguration();
        FunctionConfiguration detectFunctionCfg = new FunctionConfiguration();
        //开启人脸检测功能
        detectFunctionCfg.setSupportFaceDetect(true);
        //开启人脸识别功能
        detectFunctionCfg.setSupportFaceRecognition(true);
        //开启年龄检测功能
        detectFunctionCfg.setSupportAge(true);
        //开启性别检测功能
        detectFunctionCfg.setSupportGender(true);
        //开启活体检测功能
        detectFunctionCfg.setSupportLiveness(true);
        detectFunctionCfg.setSupportFace3dAngle(true);
        detectFunctionCfg.setSupportIRLiveness(true);
        detectFunctionCfg.setSupportImageQuality(true);
        detectFunctionCfg.setSupportMaskDetect(true);
        detectFunctionCfg.setSupportFaceLandmark(true);
        detectFunctionCfg.setSupportUpdateFaceData(true);
        detectFunctionCfg.setSupportFaceShelter(true);
        detectCfg.setFunctionConfiguration(detectFunctionCfg);

        //图片检测模式，如果是连续帧的视频流图片，那么改成VIDEO模式
        detectCfg.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        //人脸旋转角度
        detectCfg.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
        faceEngineGeneralPool = new GenericObjectPool(new FaceEngineFactory(sdkLibPath, appId, sdkKey, activeKey, detectCfg), detectPoolConfig);//底层库算法对象池


        //初始化特征比较线程池
        GenericObjectPoolConfig comparePoolConfig = new GenericObjectPoolConfig();
        comparePoolConfig.setMaxIdle(comparePooSize);
        comparePoolConfig.setMaxTotal(comparePooSize);
        comparePoolConfig.setMinIdle(comparePooSize);
        comparePoolConfig.setLifo(false);
        EngineConfiguration compareCfg = new EngineConfiguration();
        FunctionConfiguration compareFunctionCfg = new FunctionConfiguration();
        //开启人脸识别功能
        compareFunctionCfg.setSupportFaceRecognition(true);
        compareFunctionCfg.setSupportAge(true);
        compareFunctionCfg.setSupportFace3dAngle(true);
        compareFunctionCfg.setSupportFaceDetect(true);
        compareFunctionCfg.setSupportGender(true);
        compareFunctionCfg.setSupportLiveness(true);
        compareFunctionCfg.setSupportIRLiveness(true);
        compareFunctionCfg.setSupportImageQuality(true);
        compareFunctionCfg.setSupportMaskDetect(true);
        compareFunctionCfg.setSupportFaceLandmark(true);
        compareFunctionCfg.setSupportUpdateFaceData(true);
        compareFunctionCfg.setSupportFaceShelter(true);
        detectCfg.setDetectFaceMaxNum(10);
        compareCfg.setFunctionConfiguration(compareFunctionCfg);
        //图片检测模式，如果是连续帧的视频流图片，那么改成VIDEO模式
        compareCfg.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        //compareCfg.setDetectMode(DetectMode.ASF_DETECT_MODE_VIDEO);
        //人脸旋转角度
        compareCfg.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
        //底层库算法对象池
        faceEngineComparePool = new GenericObjectPool(new FaceEngineFactory(sdkLibPath, appId, sdkKey, null, compareCfg), comparePoolConfig);
        compareExecutorService = Executors.newFixedThreadPool(comparePooSize);


        List<UserFaceInfo> faceInfoList = userFaceInfoMapper.getUserFaceInfoByGroupId(101); //从数据库中取出人脸库
        if (CollectionUtil.isNotEmpty(faceInfoList)) {
            for (UserFaceInfo userFaceInfo : faceInfoList) {
                UserRamCache.addUser(userFaceInfo);
            }
        }

        //初始化IM服务器
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 实例化后记得startup哦，单独startup()的目的是让调用者可以延迟决定何时真正启动IM服务
                    final ServerLauncherImpl sli = new ServerLauncherImpl();
                    // 启动MobileIMSDK服务端的Demo
                    sli.startup();
                    // 加一个钩子，确保在JVM退出时释放netty的资源
                    Runtime.getRuntime().addShutdownHook(new Thread() {
                        @Override
                        public void run() {
                            sli.shutdown();
                        }
                    });
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }).start();
    }

    @Override
    public List<FaceDetectResDTO> detectFacesByAdd(BufferedImage bufferedImage) {
        List<FaceDetectResDTO> detectInfoList = new ArrayList<FaceDetectResDTO>();
        //加载人脸底库
        ImageInfo imageInfo = ImageFactory.bufferedImage2ImageInfo(bufferedImage);
        FaceEngine faceEngine = null;
        try {
            faceEngine = faceEngineGeneralPool.borrowObject();
            if (faceEngine == null) {
                throw new BusinessException(ErrorCodeEnum.FAIL, "获取引擎失败");
            }
            //人脸检测得到人脸列表
            List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
            //人脸检测
            int errorCode = faceEngine.detectFaces(imageInfo, faceInfoList);
            //byte[] feature = extractFaceFeature(imageInfo, faceInfoList.get(0));

            if (errorCode == 0) {
                //保存人脸
                for (FaceInfo faceInfo : faceInfoList) {
                    //截出人脸图片
                    BufferedImage headerBufImg = ImageUtil.cropImage(bufferedImage, faceInfo.getRect().left, faceInfo.getRect().top, (faceInfo.getRect().right - faceInfo.getRect().left), (faceInfo.getRect().bottom - faceInfo.getRect().top));
                    //生成文件名
                    String baseImgName = IDUtil.createID() + ".jpg";
                    //人脸大小等比例缩放
                    headerBufImg = ImageUtil.resizeBufferedImage(headerBufImg, 128, 128, true);
                    //保存人脸图片
                    ImageIO.write(headerBufImg, "jpg", new File(baseImagePath + baseImgName));

                    FaceDetectResDTO faceDetectResDTO = new FaceDetectResDTO();
                    faceDetectResDTO.setRect(faceInfo.getRect());
                    faceDetectResDTO.setFaceId(faceInfo.getFaceId());
                    faceDetectResDTO.setOrient(faceInfo.getOrient());
                    faceDetectResDTO.setWearGlasses(faceInfo.getWearGlasses());
                    faceDetectResDTO.setLeftEyeClosed(faceInfo.getLeftEyeClosed());
                    faceDetectResDTO.setRightEyeClosed(faceInfo.getRightEyeClosed());
                    faceDetectResDTO.setFaceShelter(faceInfo.getFaceShelter());
                    faceDetectResDTO.setFaceData(faceInfo.getFaceData());
                    faceDetectResDTO.setBaseImgPath(baseImgName);
                    detectInfoList.add(faceDetectResDTO);
                }
            } else {
                logger.error("人脸检测失败，errorCode：" + errorCode);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineGeneralPool.returnObject(faceEngine);
            }
        }

        return detectInfoList;

    }


    @Override
    public List<FaceInfo> detectFaces(ImageInfo imageInfo) {

        FaceEngine faceEngine = null;
        try {
            faceEngine = faceEngineGeneralPool.borrowObject();
            if (faceEngine == null) {
                throw new BusinessException(ErrorCodeEnum.FAIL, "获取引擎失败");
            }

            //人脸检测得到人脸列表
            List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
            //人脸检测
            int errorCode = faceEngine.detectFaces(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList);
            if (errorCode == 0) {
                return faceInfoList;
            } else {
                logger.error("人脸检测失败，errorCode：" + errorCode);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineGeneralPool.returnObject(faceEngine);
            }
        }

        return null;

    }

    @Override
    public Float compareFace(ImageInfo imageInfo1, ImageInfo imageInfo2) {

        List<FaceInfo> faceInfoList1 = detectFaces(imageInfo1);
        List<FaceInfo> faceInfoList2 = detectFaces(imageInfo2);

        if (CollectionUtil.isEmpty(faceInfoList1) || CollectionUtil.isEmpty(faceInfoList2)) {
            throw new BusinessException(ErrorCodeEnum.FAIL, "未检测到人脸");
        }

        byte[] feature1 = extractFaceFeature(imageInfo1, faceInfoList1.get(0));
        byte[] feature2 = extractFaceFeature(imageInfo2, faceInfoList2.get(0));

        FaceEngine faceEngine = null;
        try {
            faceEngine = faceEngineComparePool.borrowObject();
            if (faceEngine == null) {
                throw new BusinessException(ErrorCodeEnum.FAIL, "获取引擎失败");
            }

            FaceFeature faceFeature1 = new FaceFeature();
            faceFeature1.setFeatureData(feature1);
            FaceFeature faceFeature2 = new FaceFeature();
            faceFeature2.setFeatureData(feature2);
            //提取人脸特征
            FaceSimilar faceSimilar = new FaceSimilar();
            int errorCode = faceEngine.compareFaceFeature(faceFeature1, faceFeature2, faceSimilar);
            if (errorCode == 0) {
                return faceSimilar.getScore();
            } else {
                logger.error("特征提取失败，errorCode：" + errorCode);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineComparePool.returnObject(faceEngine);
            }
        }

        return null;

    }


    /**
     * 人脸特征
     *
     * @param imageInfo
     * @return
     */
    @Override
    public byte[] extractFaceFeature(ImageInfo imageInfo, FaceInfo faceInfo) {

        FaceEngine faceEngine = null;
        try {
            faceEngine = faceEngineComparePool.borrowObject();
            if (faceEngine == null) {
                throw new BusinessException(ErrorCodeEnum.FAIL, "获取引擎失败");
            }

            FaceFeature faceFeature = new FaceFeature();
            //提取人脸特征
            int errorCode = faceEngine.extractFaceFeature(imageInfo, faceInfo, ExtractType.REGISTER,0, faceFeature);
            if (errorCode == 0) {
                return faceFeature.getFeatureData();
            } else {
                logger.error("特征提取失败，errorCode：" + errorCode);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineComparePool.returnObject(faceEngine);
            }
        }

        return null;

    }


    @Override
    public List<UserCompareInfo> faceRecognition(byte[] faceFeature, List<UserFaceInfo> userInfoList, float passRate) {
        //识别到的人脸列表
        List<UserCompareInfo> resultUserInfoList = Lists.newLinkedList();

        FaceFeature targetFaceFeature = new FaceFeature();
        targetFaceFeature.setFeatureData(faceFeature);

        //分成1000一组，多线程处理
        List<List<UserFaceInfo>> faceUserInfoPartList = Lists.partition(userInfoList, 1000);
        CompletionService<List<UserCompareInfo>> completionService = new ExecutorCompletionService(compareExecutorService);
        for (List<UserFaceInfo> part : faceUserInfoPartList) {
            completionService.submit(new CompareFaceTask(part, targetFaceFeature, passRate));
        }
        for (int i = 0; i < faceUserInfoPartList.size(); i++) {
            List<UserCompareInfo> faceUserInfoList = null;
            try {
                faceUserInfoList = completionService.take().get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error(e.getMessage());
            }
            if (CollectionUtil.isNotEmpty(userInfoList)) {
                resultUserInfoList.addAll(faceUserInfoList);
            }
        }
        //从大到小排序
        resultUserInfoList.sort((h1, h2) -> h2.getSimilar().compareTo(h1.getSimilar()));

        return resultUserInfoList;
    }

    @Override
    public List<ProcessInfo> process(ImageInfo imageInfo, List<FaceInfo> faceInfoList) {
        FaceEngine faceEngine = null;
        try {
            //获取引擎对象
            faceEngine = faceEngineGeneralPool.borrowObject();
            if (faceEngine == null) {
                throw new BusinessException(ErrorCodeEnum.FAIL, "获取引擎失败");
            }


            int errorCode = faceEngine.process(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList, FunctionConfiguration.builder().supportLiveness(true).supportAge(true).supportGender(true).build());
            if (errorCode == 0) {
                List<ProcessInfo> processInfoList = Lists.newLinkedList();
                //活体检测
                List<LivenessInfo> livenessInfoList = new ArrayList<LivenessInfo>();
                faceEngine.getLiveness(livenessInfoList);

                //性别列表
                List<GenderInfo> genderInfoList = new ArrayList<GenderInfo>();
                faceEngine.getGender(genderInfoList);

                //年龄列表
                List<AgeInfo> ageInfoList = new ArrayList<AgeInfo>();
                faceEngine.getAge(ageInfoList);

                for (int i = 0; i < genderInfoList.size(); i++) {
                    ProcessInfo processInfo = new ProcessInfo();
                    if (livenessInfoList.size() >= i + 1) {
                        processInfo.setLiveness(livenessInfoList.get(i).getLiveness());
                    }
                    if (ageInfoList.size() >= i + 1) {
                        processInfo.setAge(ageInfoList.get(i).getAge());
                    }
                    processInfo.setGender(genderInfoList.get(i).getGender());
                    processInfoList.add(processInfo);
                }
                return processInfoList;
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineGeneralPool.returnObject(faceEngine);
            }
        }

        return null;

    }


    private class CompareFaceTask implements Callable<List<UserCompareInfo>> {

        private List<UserFaceInfo> userInfoList;
        private FaceFeature targetFaceFeature;
        private float passRate;


        public CompareFaceTask(List<UserFaceInfo> userInfoList, FaceFeature targetFaceFeature, float passRate) {
            this.userInfoList = userInfoList;
            this.targetFaceFeature = targetFaceFeature;
            this.passRate = passRate;
        }

        @Override
        public List<UserCompareInfo> call() throws Exception {
            FaceEngine faceEngine = null;
            //识别到的人脸列表
            List<UserCompareInfo> resultUserInfoList = Lists.newLinkedList();
            try {
                faceEngine = faceEngineComparePool.borrowObject();
                for (UserFaceInfo userInfo : userInfoList) {
                    FaceFeature sourceFaceFeature = new FaceFeature();
                    sourceFaceFeature.setFeatureData(userInfo.getFaceFeature());
                    FaceSimilar faceSimilar = new FaceSimilar();
                    faceEngine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
                    if (faceSimilar.getScore() > passRate) {//相似值大于配置预期，加入到识别到人脸的列表
                        UserCompareInfo info = new UserCompareInfo();
                        info.setName(userInfo.getName());
                        info.setFaceId(userInfo.getFaceId());
                        info.setBaseImgPath(userInfo.getBaseImgPath());
                        info.setSimilar(faceSimilar.getScore());
                        resultUserInfoList.add(info);
                    }
                }
            } catch (Exception e) {
                logger.error("", e);
            } finally {
                if (faceEngine != null) {
                    faceEngineComparePool.returnObject(faceEngine);
                }
            }

            return resultUserInfoList;
        }

    }

    @PreDestroy
    public void destroy() {
        if (faceEngineGeneralPool != null) {
            faceEngineGeneralPool.close();
        }
        if (faceEngineComparePool != null) {
            faceEngineComparePool.close();
        }
        logger.info("连接池已关闭");
    }
}
