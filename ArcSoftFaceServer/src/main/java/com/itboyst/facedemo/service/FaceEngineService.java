package com.itboyst.facedemo.service;


import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.toolkit.ImageInfo;
import com.itboyst.facedemo.dto.FaceDetectResDTO;
import com.itboyst.facedemo.dto.ProcessInfo;
import com.itboyst.facedemo.dto.UserCompareInfo;
import com.itboyst.facedemo.entity.UserFaceInfo;

import java.awt.image.BufferedImage;
import java.util.List;


public interface FaceEngineService {
    /**
     * 检查人脸并保存
     * @param bufferedImage
     * @return
     */
    List<FaceDetectResDTO> detectFacesByAdd(BufferedImage bufferedImage);
    /**
     * 检查人脸
     * @param imageInfo
     * @return
     */
    List<FaceInfo> detectFaces(ImageInfo imageInfo);

    /**
     * 比较人脸
     * @param imageInfo1
     * @param imageInfo2
     * @return
     */
    Float compareFace(ImageInfo imageInfo1,ImageInfo imageInfo2) ;

    /**
     * 人脸特征提取
     * @param imageInfo
     * @param faceInfo
     * @return
     */
    byte[] extractFaceFeature(ImageInfo imageInfo,FaceInfo faceInfo);


    /**
     * 人脸对比
     * @param faceFeature
     * @param userInfoList
     * @param passRate
     * @return
     */
    List<UserCompareInfo> faceRecognition(byte[] faceFeature, List<UserFaceInfo> userInfoList, float passRate) ;

    /**
     * 检测年龄,性别等初始化
     * @param imageInfo
     * @param faceInfoList
     * @return
     */
    List<ProcessInfo> process(ImageInfo imageInfo,List<FaceInfo> faceInfoList);





}
