package com.itboyst.facedemo.service.impl;

import com.itboyst.facedemo.entity.UserFaceInfo;
import com.itboyst.facedemo.mapper.UserFaceInfoMapper;
import com.itboyst.facedemo.service.UserFaceInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class UserFaceInfoServiceImpl implements UserFaceInfoService {


    @Resource
    private UserFaceInfoMapper userFaceInfoMapper;

    @Override
    public void insertSelective(UserFaceInfo userFaceInfo) {
        userFaceInfoMapper.insertUserFaceInfo(userFaceInfo);
    }
}
