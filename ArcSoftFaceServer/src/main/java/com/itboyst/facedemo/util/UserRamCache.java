package com.itboyst.facedemo.util;

import com.google.common.collect.Lists;
import com.itboyst.facedemo.entity.UserFaceInfo;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author st7251
 * @date 2020/3/13 16:34
 */
public class UserRamCache {

    private static  ConcurrentHashMap<Integer, UserFaceInfo> userInfoMap = new ConcurrentHashMap<>();

    public static void addUser(UserFaceInfo userInfo) {
        userInfoMap.put(userInfo.getFaceId(), userInfo);
    }

    public static void removeUser(String faceId) {
        userInfoMap.remove(faceId);
    }

    public static List<UserFaceInfo> getUserList() {
        List<UserFaceInfo> userInfoList = Lists.newLinkedList();
        for (UserFaceInfo value : userInfoMap.values()) {
            userInfoList.add(value);
        }
        return userInfoList;
    }
}
