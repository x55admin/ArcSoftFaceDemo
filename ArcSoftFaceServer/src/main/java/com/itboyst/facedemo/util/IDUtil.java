package com.itboyst.facedemo.util;

/**
 * 生成唯一id
 * @author: x55admin ming1332236@126.com
 * @date: 2020/5/11 10:27.
 */
public class IDUtil {
    private static final byte[] LOCK = new byte[0];

    // 位数，默认是2位
    private final static long digits = 10;

    public static String createID() {
        long r = 0;
        synchronized (LOCK) {
            r = (long) ((Math.random() + 1) * digits);
        }
        return System.currentTimeMillis() + String.valueOf(r).substring(1);
    }

    public static void main(String[] args) {
        System.out.println(createID());
    }
}
