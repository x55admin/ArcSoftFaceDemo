package com.itboyst.facedemo.util;

import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.Rect;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ShowVideo {

    static {
        String basePath = "F:\\more\\face\\arcsoft\\ArcSoftFaceDemo_4.0\\lib\\";
        System.load(basePath + "opencv_java320.dll");
        System.load(basePath + "opencv_ffmpeg320_64.dll");
        System.load(basePath + "opencv_world320.dll");
    }

    /**
     * 人脸框的位置
     *
     * @param faceInfos
     * @return
     */
    public static List<RotatedRect> boundingBox(List<FaceInfo> faceInfos) {
        List<RotatedRect> rotatedRectList = new ArrayList<RotatedRect>();
        if (faceInfos != null) {
            for (FaceInfo faceInfo : faceInfos) {
                Rect rect = faceInfo.getRect();
                Point center = new Point();
                int width = 0;
                if (rect != null) {
                    width = rect.right - rect.left;
                    center = new Point((rect.left + rect.right) / 2, (rect.top + rect.bottom) / 2);
                }
                float angle = 0;
                rotatedRectList.add(new RotatedRect(center, new Size(width, width), angle));
            }
        }
        return rotatedRectList;
    }

    /**
     * 画人脸框
     *
     * @param img
     * @param boxs
     * @param color
     */
    public static void drawRotatedBox(Mat img, List<RotatedRect> boxs, Scalar color) {
        if (boxs != null) {
            for (RotatedRect box : boxs) {
                Point[] vertices = new Point[4];
                box.points(vertices);
                for (int j = 0; j < 4; j++) {
                    Imgproc.line(img, vertices[j], vertices[(j + 1) % 4], color);
                }
            }
        }
    }

    public static BufferedImage conver2Image(Mat mat) {
        int width = mat.cols();
        int height = mat.rows();
        int dims = mat.channels();
        int[] pixels = new int[width * height];
        byte[] rgbdata = new byte[width * height * dims];
        mat.get(0, 0, rgbdata);
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        int index = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if (dims == 3) {
                    index = row * width * dims + col * dims;
                    b = rgbdata[index] & 0xff;
                    g = rgbdata[index + 1] & 0xff;
                    r = rgbdata[index + 2] & 0xff;
                    pixels[row * width + col] =
                            ((255 & 0xff) << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | b & 0xff;
                }
                if (dims == 1) {
                    index = row * width + col;
                    b = rgbdata[index] & 0xff;
                    pixels[row * width + col] =
                            ((255 & 0xff) << 24) | ((b & 0xff) << 16) | ((b & 0xff) << 8) | b & 0xff;
                }
            }
        }
        setRGB(image, 0, 0, width, height, pixels);
        return image;
    }

    public static void setRGB(BufferedImage image, int x, int y, int width, int height, int[] pixels) {
        int type = image.getType();
        if (type == BufferedImage.TYPE_INT_ARGB || type == BufferedImage.TYPE_INT_RGB) {
            image.getRaster().setDataElements(x, y, width, height, pixels);
        } else {
            image.setRGB(x, y, width, height, pixels, 0, width);
        }
    }

    public static String getProperties(String filePath, String keyWord) {
        Properties prop = null;
        String value = null;
        try {
            // 通过Spring中的PropertiesLoaderUtils工具类进行获取
            prop = PropertiesLoaderUtils.loadAllProperties(filePath);
            // 根据关键字查询相应的值
            value = prop.getProperty(keyWord);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }

}
