package com.itboyst.facedemo;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.RandomUtil;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.itboyst.facedemo.dto.FaceDetectResDTO;
import com.itboyst.facedemo.entity.UserFaceInfo;
import com.itboyst.facedemo.service.FaceEngineService;
import com.itboyst.facedemo.service.UserFaceInfoService;
import com.itboyst.facedemo.util.ShowVideo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.itboyst.facedemo.util.ImageUtil.getImgInfo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FaceBatchAddTest {

    @Autowired
    UserFaceInfoService userFaceInfoService;
    @Autowired
    FaceEngineService faceEngineService;

    @Test
    public void faceBatchAdd() {
        int errorCode;
        int baseImgCount = 0;
        String baseImagePath = ShowVideo.getProperties("application.properties", "config.arcface-sdk.base-image-path");
        System.out.println("------------------" + baseImagePath);
        //人脸特征获取
        Map<String, File> imgInfo = getImgInfo(baseImagePath);
        for (Map.Entry<String, File> entry : imgInfo.entrySet()) {
            String name = entry.getKey();
            File file = entry.getValue();
            try {
                BufferedImage bufferedImage = ImageIO.read(file);
                //加载人脸底库
                ImageInfo rgbData = ImageFactory.getRGBData(file);
                List<FaceDetectResDTO> faceDetectResDTOS = faceEngineService.detectFacesByAdd(bufferedImage);
                if (CollectionUtil.isNotEmpty(faceDetectResDTOS) && faceDetectResDTOS.size() > 0) {
                    List<FaceInfo> faceInfoList = new ArrayList<>();
                    for (FaceDetectResDTO faceDetectResDTO : faceDetectResDTOS) {
                        FaceInfo faceInfo = new FaceInfo();
                        faceInfo.setRect(faceDetectResDTO.getRect());
                        faceInfo.setFaceId(faceDetectResDTO.getFaceId());
                        faceInfo.setOrient(faceDetectResDTO.getOrient());
                        faceInfo.setWearGlasses(faceDetectResDTO.getWearGlasses());
                        faceInfo.setLeftEyeClosed(faceDetectResDTO.getLeftEyeClosed());
                        faceInfo.setRightEyeClosed(faceDetectResDTO.getRightEyeClosed());
                        faceInfo.setFaceShelter(faceDetectResDTO.getFaceShelter());
                        faceInfo.setFaceData(faceDetectResDTO.getFaceData());
                        faceInfoList.add(faceInfo);
                    }
                    byte[] feature = faceEngineService.extractFaceFeature(rgbData, faceInfoList.get(0));
                    UserFaceInfo userFaceInfo = new UserFaceInfo();
                    userFaceInfo.setName(name);
                    userFaceInfo.setGroupId(101);
                    userFaceInfo.setFaceFeature(feature);
                    userFaceInfo.setBaseImgPath(faceDetectResDTOS.get(0).getBaseImgPath());
                    userFaceInfo.setFaceId(RandomUtil.randomInt(1000000));

                    //人脸特征插入到数据库
                    userFaceInfoService.insertSelective(userFaceInfo);
                }
                baseImgCount++;
                System.out.println("baseFeats:" + baseImgCount + "/" + imgInfo.size());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
