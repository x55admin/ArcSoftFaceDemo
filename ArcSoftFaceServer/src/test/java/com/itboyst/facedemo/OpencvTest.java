package com.itboyst.facedemo;

import cn.hutool.core.collection.CollectionUtil;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.Rect;
import com.arcsoft.face.toolkit.ImageInfo;
import com.google.common.collect.Lists;
import com.itboyst.facedemo.dto.FaceDetectResDTO;
import com.itboyst.facedemo.dto.ProcessInfo;
import com.itboyst.facedemo.dto.UserCompareInfo;
import com.itboyst.facedemo.service.FaceEngineService;
import com.itboyst.facedemo.service.UserFaceInfoService;
import com.itboyst.facedemo.util.ImageGUI;
import com.itboyst.facedemo.util.ImageUtil;
import com.itboyst.facedemo.util.ShowVideo;
import com.itboyst.facedemo.util.UserRamCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

import static com.arcsoft.face.toolkit.ImageFactory.bufferedImage2ImageInfo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpencvTest {

    @Autowired
    UserFaceInfoService userFaceInfoService;
    @Autowired
    FaceEngineService faceEngineService;

    @Test
    public void testOpencv() {
        String baseLibPath = ShowVideo.getProperties("application.properties", "config.arcface-sdk.jar-path");
        System.load(baseLibPath + "opencv_java320.dll");
        System.load(baseLibPath + "opencv_ffmpeg320_64.dll");
        System.load(baseLibPath + "opencv_world320.dll");

        // 打开摄像头或者视频文件
        // device为0默认打开笔记本电脑自带摄像头，若为0打不开外置usb摄像头
        // 请把device修改为1或2再重试，1，或2为usb插上电脑后，电脑认可的usb设备id
        VideoCapture capture = new VideoCapture();
        //capture.open(0);
        capture.open("C:\\ArcSoftFaceDemo\\ArcSoftFaceServer\\img\\vip2.mp4");
        if (!capture.isOpened()) {
            System.out.println("could not load video data...");
            return;
        }
        int frameWidth = (int) capture.get(3);
        //int frameWidth = 720;
        int frameHeight = (int) capture.get(4);
        //int frameHeight = 480;
        ImageGUI gui = new ImageGUI();
        gui.createWin("camera", new Dimension(frameWidth, frameHeight));
        Mat frame = new Mat();
        List<FaceDetectResDTO> faceDetectResDTOS = Lists.newLinkedList();
        int index = 0;
        while (true) {
            boolean have = capture.read(frame);
            // Win上摄像头
            Core.flip(frame, frame, 1);
            if (!have) {
                break;
            }
            if (!frame.empty()) {
                BufferedImage bufferedImage = ImageUtil.mat2BufImg(frame, ".jpg");
                if (bufferedImage != null) {
                    //视频转换为图片
                    long start = System.currentTimeMillis();
                    ImageInfo videoImageInfo = bufferedImage2ImageInfo(bufferedImage);
                    //特征视频图片提取
                    List<FaceInfo> videoFaceInfoList = faceEngineService.detectFaces(videoImageInfo);
                    List<ProcessInfo> process = faceEngineService.process(videoImageInfo, videoFaceInfoList);
                    if (videoFaceInfoList!=null && videoFaceInfoList.size() > 0) {
                        index++;
                        if (index >= 5) {
                            //特征比对
                            faceDetectResDTOS.clear();
                            for (int i = 0; i < videoFaceInfoList.size(); i++) {
                                FaceDetectResDTO faceDetectResDTO = new FaceDetectResDTO();
                                FaceInfo faceInfo = videoFaceInfoList.get(i);
                                faceDetectResDTO.setRect(faceInfo.getRect());
                                faceDetectResDTO.setFaceId(faceInfo.getFaceId());
                                faceDetectResDTO.setOrient(faceInfo.getOrient());
                                faceDetectResDTO.setWearGlasses(faceInfo.getWearGlasses());
                                faceDetectResDTO.setLeftEyeClosed(faceInfo.getLeftEyeClosed());
                                faceDetectResDTO.setRightEyeClosed(faceInfo.getRightEyeClosed());
                                faceDetectResDTO.setFaceShelter(faceInfo.getFaceShelter());
                                faceDetectResDTO.setFaceData(faceInfo.getFaceData());
                                if (CollectionUtil.isNotEmpty(process)) {
                                    ProcessInfo processInfo = process.get(i);
                                    faceDetectResDTO.setAge(processInfo.getAge());
                                    faceDetectResDTO.setGender(processInfo.getGender());
                                    faceDetectResDTO.setLiveness(processInfo.getLiveness());
                                }
                                byte[] feature = faceEngineService.extractFaceFeature(videoImageInfo, faceInfo);

                                if (feature != null) {
                                    List<UserCompareInfo> userCompareInfos = faceEngineService.faceRecognition(feature, UserRamCache.getUserList(), 0.8f);
                                    if (CollectionUtil.isNotEmpty(userCompareInfos)) {
                                        faceDetectResDTO.setName(userCompareInfos.get(0).getName());
                                        faceDetectResDTO.setSimilar(userCompareInfos.get(0).getSimilar());
                                        faceDetectResDTO.setFaceId(userCompareInfos.get(0).getFaceId());
                                    }
                                }
                                long end = System.currentTimeMillis();
                                System.out.println("检测耗时:" + (end - start) + "ms");
                                faceDetectResDTOS.add(faceDetectResDTO);


                            }
                            //System.out.println(index);
                            index = 0;
                        }
                        //标记人脸信息
                        for (FaceDetectResDTO faceDetectResDTO : faceDetectResDTOS) {
                            int faceId = faceDetectResDTO.getFaceId();
                            String name = faceDetectResDTO.getName();
                            float score = faceDetectResDTO.getSimilar();
                            Rect rect = faceDetectResDTO.getRect();
                            int age = faceDetectResDTO.getAge();
                            int gender = faceDetectResDTO.getGender();
                            int liveness = faceDetectResDTO.getLiveness();
                            String genderStr = gender == 0 ? "男" : "女";
                            String livenessStr = (liveness == 1 ? "活体" : "非活体");
                            //Imgproc.putText(frame, name + " " + String.valueOf(score), new Point(rect.left, rect.top), 0, 1.0, new Scalar(0, 255, 0), 1, Imgproc.LINE_AA, false);
                            frame = ImageUtil.addMark(bufferedImage, videoFaceInfoList, faceId, name, score, rect, age, genderStr, livenessStr);
                        }
                    }
                    gui.imshow(ShowVideo.conver2Image(frame));
                    gui.repaint();
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
