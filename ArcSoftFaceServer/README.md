## 应用场景:
将标记的人脸信息,通过虹软sdk实现人脸识别,将识别到的结果发送到android客户端,通知业务人员,从而第一时间获取客户信息,可以用迎宾系统,商超特殊人员通知系统等.
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210716102332993.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21pbmcxMzMyMg==,size_16,color_FFFFFF,t_70#)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210716102358373.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21pbmcxMzMyMg==,size_16,color_FFFFFF,t_70#pic_center)
效果如下:

[video(video-v31m0yK7-1626403324361)(type-bilibili)(url-https://player.bilibili.com/player.html?aid=931654118)(image-https://ss.csdn.net/p?http://i1.hdslb.com/bfs/archive/22f423fb530368393a6bb11f6fdeae676a6f0407.jpg)(title-人脸识别迎宾系统)]



## 功能点
* 人脸识别
* 人脸底库批量入库
* 视频或摄像头人脸识别功能
* Android端实时接收人脸信息
* Android语音播报人脸信息
## 开发工具
* IDEA
* Android Studio
* Navicat for mysql

## 技术架构
* java 8
* spring boot
* mysql 5.7+
* android
* 虹软人脸识别sdk增值版 java 4.0
## 运行须知
* 1.首先在mysql中创建arc_face_base 数据库,数据库编码为utf8mb4,然后导入doc中user_face_info.sql的内容
* 2.到虹软官网http://ai.arcsoft.com.cn/ 免费申请下载SDK,增值版支持试用,将申请的引擎库libarcsoft_face、libarcsoft_face_engine、libarcsoft_face_engine_jni下载到指定文件夹备用,注意区分X86和X64，和当前jdk版本一致。
* 3.git下载服务器端源码,更改为自己的配置信息,首先更改修改配置文件*src\main\resources\application.properties*
人脸识别引擎库存放路径：*config.arcface-sdk.sdk-lib-path*
人脸底库存放地址:*config.arcface-sdk.base-image-path*
填写人脸识别id：*config.arcface-sdk.app-id*
填写人脸识别key：*config.arcface-sdk.sdk-key*

数据库连接: *spring.datasource.druid.url*
数据库用户名: *spring.datasource.druid.username*
数据库密码: *spring.datasource.druid.password*

可以用com.itboyst.facedemo.FaceEngineTest测试SDK是否能正常运行
```
package com.itboyst.facedemo;
import com.arcsoft.face.*;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectModel;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ExtractType;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.arcsoft.face.toolkit.ImageInfoEx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
public class FaceEngineTest {
    public static void main(String[] args) {

        //激活码，从官网获取
        String appId = "";
        String sdkKey = "";
        String activeKey = "";

        System.err.println("注意，如果返回的errorCode不为0，可查看com.arcsoft.face.enums.ErrorInfo类获取相应的错误信息");

        //人脸识别引擎库存放路径
        FaceEngine faceEngine = new FaceEngine("F:\\WIN64");
        //激活引擎
        int errorCode = faceEngine.activeOnline(appId, sdkKey, activeKey);
        System.out.println("引擎激活errorCode:" + errorCode);


        ActiveDeviceInfo activeDeviceInfo = new ActiveDeviceInfo();
        //采集设备信息（可离线）
        errorCode = faceEngine.getActiveDeviceInfo(activeDeviceInfo);
        System.out.println("采集设备信息errorCode:" + errorCode);
        System.out.println("设备信息:"+activeDeviceInfo.getDeviceInfo());

        //faceEngine.activeOffline("d:\\ArcFacePro64.dat.offline");

        //ActiveFileInfo activeFileInfo = new ActiveFileInfo();
        //errorCode = faceEngine.getActiveFileInfo(activeFileInfo);
        //System.out.println("获取激活文件errorCode:" + errorCode);
        //System.out.println("激活文件信息:" + activeFileInfo.toString());

        //引擎配置
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
        engineConfiguration.setDetectFaceMaxNum(10);
        //功能配置
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportAge(true);
        functionConfiguration.setSupportFace3dAngle(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportGender(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        functionConfiguration.setSupportImageQuality(true);
        functionConfiguration.setSupportMaskDetect(true);
        functionConfiguration.setSupportFaceLandmark(true);
        functionConfiguration.setSupportUpdateFaceData(true);
        functionConfiguration.setSupportFaceShelter(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);

        //初始化引擎
        errorCode = faceEngine.init(engineConfiguration);
        System.out.println("初始化引擎errorCode:" + errorCode);


        //人脸检测
        ImageInfo imageInfo = ImageFactory.getRGBData(new File("xxx.jpg"));
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        errorCode = faceEngine.detectFaces(imageInfo, faceInfoList);
        System.out.println("人脸检测errorCode:" + errorCode);
        System.out.println("检测到人脸数:" + faceInfoList.size());


        ImageQuality imageQuality = new ImageQuality();
        errorCode = faceEngine.imageQualityDetect(imageInfo, faceInfoList.get(0),0, imageQuality);
        System.out.println("图像质量检测errorCode:" + errorCode);
        System.out.println("图像质量分数:" + imageQuality.getFaceQuality());

        //特征提取
        FaceFeature faceFeature = new FaceFeature();
        errorCode = faceEngine.extractFaceFeature(imageInfo, faceInfoList.get(0), ExtractType.REGISTER,0, faceFeature);
        System.out.println("特征提取errorCode:" + errorCode);

        //人脸检测2
        ImageInfo imageInfo2 = ImageFactory.getRGBData(new File("xxx.jpg"));
        List<FaceInfo> faceInfoList2 = new ArrayList<FaceInfo>();
        errorCode = faceEngine.detectFaces(imageInfo2, faceInfoList2);
        System.out.println("人脸检测errorCode:" + errorCode);
        System.out.println("检测到人脸数:" + faceInfoList.size());

        //特征提取2
        FaceFeature faceFeature2 = new FaceFeature();
        errorCode = faceEngine.extractFaceFeature(imageInfo2, faceInfoList.get(0), ExtractType.REGISTER,0, faceFeature);
        System.out.println("特征提取errorCode:" + errorCode);

        //特征比对
        FaceFeature targetFaceFeature = new FaceFeature();
        targetFaceFeature.setFeatureData(faceFeature.getFeatureData());
        FaceFeature sourceFaceFeature = new FaceFeature();
        sourceFaceFeature.setFeatureData(faceFeature2.getFeatureData());
        FaceSimilar faceSimilar = new FaceSimilar();

        errorCode = faceEngine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
        System.out.println("特征比对errorCode:" + errorCode);
        System.out.println("人脸相似度：" + faceSimilar.getScore());


        //人脸属性检测
        FunctionConfiguration configuration = new FunctionConfiguration();
        configuration.setSupportAge(true);
        configuration.setSupportFace3dAngle(true);
        configuration.setSupportGender(true);
        configuration.setSupportLiveness(true);
        configuration.setSupportMaskDetect(true);
        configuration.setSupportFaceLandmark(true);
        errorCode = faceEngine.process(imageInfo, faceInfoList, configuration);
        System.out.println("图像属性处理errorCode:" + errorCode);

        //性别检测
        List<GenderInfo> genderInfoList = new ArrayList<GenderInfo>();
        errorCode = faceEngine.getGender(genderInfoList);
        System.out.println("性别：" + genderInfoList.get(0).getGender());

        //年龄检测
        List<AgeInfo> ageInfoList = new ArrayList<AgeInfo>();
        errorCode = faceEngine.getAge(ageInfoList);
        System.out.println("年龄：" + ageInfoList.get(0).getAge());

        //3D信息检测
        List<Face3DAngle> face3DAngleList = new ArrayList<Face3DAngle>();
        errorCode = faceEngine.getFace3DAngle(face3DAngleList);
        System.out.println("3D角度：" + face3DAngleList.get(0).getPitch() + "," + face3DAngleList.get(0).getRoll() + "," + face3DAngleList.get(0).getYaw());

        //活体检测
        List<LivenessInfo> livenessInfoList = new ArrayList<LivenessInfo>();
        errorCode = faceEngine.getLiveness(livenessInfoList);
        System.out.println("活体：" + livenessInfoList.get(0).getLiveness());


        //Landmark检测
        List<LandmarkInfo> landmarkInfoList = new ArrayList<LandmarkInfo>();
        errorCode = faceEngine.getLandmark(landmarkInfoList);
        System.out.println("Landmark：" + landmarkInfoList.get(0).getLandmarks()[0].getX());

        //口罩检测
        List<MaskInfo> maskInfoList = new ArrayList<MaskInfo>();
        errorCode = faceEngine.getMask(maskInfoList);
        System.out.println("口罩：" + maskInfoList.get(0).getMask());


        //IR属性处理
        ImageInfo imageInfoGray = ImageFactory.getGrayData(new File("xxx.jpg"));
        List<FaceInfo> faceInfoListGray = new ArrayList<FaceInfo>();
        errorCode = faceEngine.detectFaces(imageInfoGray, faceInfoListGray);

        FunctionConfiguration configuration2 = new FunctionConfiguration();
        configuration2.setSupportIRLiveness(true);
        errorCode = faceEngine.processIr(imageInfoGray, faceInfoListGray, configuration2);
        //IR活体检测
        List<IrLivenessInfo> irLivenessInfo = new ArrayList();
        errorCode = faceEngine.getLivenessIr(irLivenessInfo);
        System.out.println("IR活体：" + irLivenessInfo.get(0).getLiveness());


        //获取激活文件信息
        ActiveFileInfo activeFileInfo2 = new ActiveFileInfo();
        errorCode = faceEngine.getActiveFileInfo(activeFileInfo2);

        //更新人脸数据
        errorCode = faceEngine.updateFaceData(imageInfo, faceInfoList);

        //高级人脸图像处理接口
        ImageInfoEx imageInfoEx = new ImageInfoEx();
        imageInfoEx.setHeight(imageInfo.getHeight());
        imageInfoEx.setWidth(imageInfo.getWidth());
        imageInfoEx.setImageFormat(imageInfo.getImageFormat());
        imageInfoEx.setImageDataPlanes(new byte[][]{imageInfo.getImageData()});
        imageInfoEx.setImageStrides(new int[]{imageInfo.getWidth() * 3});
        List<FaceInfo> faceInfoList1 = new ArrayList();
        errorCode = faceEngine.detectFaces(imageInfoEx, DetectModel.ASF_DETECT_MODEL_RGB, faceInfoList1);
        ImageQuality imageQuality1=new ImageQuality();
        errorCode = faceEngine.imageQualityDetect(imageInfoEx, faceInfoList1.get(0),0, imageQuality1);
        FunctionConfiguration fun = new FunctionConfiguration();
        fun.setSupportAge(true);
        errorCode = faceEngine.process(imageInfoEx, faceInfoList1, fun);
        List<AgeInfo> ageInfoList1 = new ArrayList();
        int age = faceEngine.getAge(ageInfoList1);
        FaceFeature feature = new FaceFeature();
        errorCode = faceEngine.extractFaceFeature(imageInfoEx, faceInfoList1.get(0), ExtractType.REGISTER,0,feature);
        errorCode = faceEngine.updateFaceData(imageInfoEx,faceInfoList1);

        //设置活体测试
        errorCode = faceEngine.setLivenessParam(0.5f, 0.7f);
        System.out.println("设置活体活体阈值errorCode:" + errorCode);

        errorCode=faceEngine.setFaceShelterParam(0.8f);
        System.out.println("设置设置人脸遮挡阈值errorCode:" + errorCode);

        //引擎卸载
        errorCode = faceEngine.unInit();

    }
}
```

# 人脸底库批量入库
```
@RunWith(SpringRunner.class)
@SpringBootTest
public class FaceBatchAddTest {

    @Autowired
    UserFaceInfoService userFaceInfoService;
    @Autowired
    FaceEngineService faceEngineService;

    @Test
    public void faceBatchAdd() {
        int errorCode;
        int baseImgCount = 0;
        //人脸特征获取
        Map<String, File> imgInfo = getImgInfo("此处设置底库图片路径,底库照片最好为一寸照形式,照片名字为此人名称*");
        for (Map.Entry<String, File> entry : imgInfo.entrySet()) {
            String name = entry.getKey();
            File file = entry.getValue();
            try {
                BufferedImage bufferedImage = ImageIO.read(file);
                //加载人脸底库
                ImageInfo rgbData = ImageFactory.getRGBData(file);
                List<FaceDetectResDTO> faceDetectResDTOS = faceEngineService.detectFacesByAdd(bufferedImage);
                if (CollectionUtil.isNotEmpty(faceDetectResDTOS) && faceDetectResDTOS.size() > 0) {
                    List<FaceInfo> faceInfoList = new ArrayList<>();
                    for (FaceDetectResDTO faceDetectResDTO : faceDetectResDTOS) {
                        FaceInfo faceInfo = new FaceInfo();
                        faceInfo.setRect(faceDetectResDTO.getRect());
                        faceInfo.setFaceId(faceDetectResDTO.getFaceId());
                        faceInfo.setOrient(faceDetectResDTO.getOrient());
                        faceInfo.setWearGlasses(faceDetectResDTO.getWearGlasses());
                        faceInfo.setLeftEyeClosed(faceDetectResDTO.getLeftEyeClosed());
                        faceInfo.setRightEyeClosed(faceDetectResDTO.getRightEyeClosed());
                        faceInfo.setFaceShelter(faceDetectResDTO.getFaceShelter());
                        faceInfo.setFaceData(faceDetectResDTO.getFaceData());
                        faceInfoList.add(faceInfo);
                    }
                    byte[] feature = faceEngineService.extractFaceFeature(rgbData, faceInfoList.get(0));
                    UserFaceInfo userFaceInfo = new UserFaceInfo();
                    userFaceInfo.setName(name);
                    userFaceInfo.setGroupId(101);
                    userFaceInfo.setFaceFeature(feature);
                    userFaceInfo.setBaseImgPath(faceDetectResDTOS.get(0).getBaseImgPath());
                    userFaceInfo.setFaceId(RandomUtil.randomInt(1000000));

                    //人脸特征插入到数据库
                    userFaceInfoService.insertSelective(userFaceInfo);
                }
                baseImgCount++;
                System.out.println("baseFeats:" + baseImgCount + "/" + imgInfo.size());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
```

底库样式如下
![例如这个名称设置为冯一曼](https://img-blog.csdnimg.cn/20210716102212105.jpg#)
配置完成后执行faceBatchAdd方法.注意如果报错
``
java.awt.HeadlessException
	at java.awt.GraphicsEnvironment.checkHeadless(GraphicsEnvironment.java:204)
	at java.awt.Window.<init>(Window.java:536)
	at java.awt.Frame.<init>(Frame.java:420)
	at java.awt.Frame.<init>(Frame.java:385)
	at javax.swing.SwingUtilities$SharedOwnerFrame.<init>(SwingUtilities.java:1763)
	at javax.swing.SwingUtilities.getSharedOwnerFrame(SwingUtilities.java:1838)
	at javax.swing.JDialog.<init>(JDialog.java:272)
	at javax.swing.JDialog.<init>(JDialog.java:206)
	at javax.swing.JDialog.<init>(JDialog.java:154)
	at com.itboyst.facedemo.util.ImageGUI.createWin(ImageGUI.java:40)
	at com.itboyst.facedemo.OpencvTest.testOpencv(OpencvTest.java:65)
``
解决办法:在执行类的jvm options上加 -Djava.awt.headless=false
在数据库查看人脸信息是否入库

正常入库后就可以通过com.itboyst.facedemo.Application 启动项目,项目启动后在浏览器中 打开http://127.0.0.1:8099/ 访问项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210716104443313.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21pbmcxMzMyMg==,size_16,color_FFFFFF,t_70#pic_center)
此时上传底库中存在的人脸照片信息,就可以识别出来.
也可以在下图所示位置单张设置人脸底库信息,以备日后使用

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210716104645331.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21pbmcxMzMyMg==,size_16,color_FFFFFF,t_70#pic_center)
## Android客户端部署
git下载anroid源码,在android studio中打开项目并编译部署到手机上,
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210716105346130.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21pbmcxMzMyMg==,size_16,color_FFFFFF,t_70#pic_center)
此处用户名密码都为1,然后点击登陆,如果正常进入下个界面就可以接收人脸识别结果了.如果登陆不成功检查服务器ip是否为服务器ip,服务器上的spring boot项目是否正常启动,服务器防火墙是否打开8888端口.
登陆成功后就可以通过下图位置上传照片就可以在android接收到人脸识别的信息了,还可以听到声音![在这里插入图片描述](https://img-blog.csdnimg.cn/20210716105743124.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L21pbmcxMzMyMg==,size_16,color_FFFFFF,t_70#pic_center)

如下效果
[video(video-LOYRfBZm-1626405182658)(type-bilibili)(url-https://player.bilibili.com/player.html?aid=931654118)(image-https://ss.csdn.net/p?http://i1.hdslb.com/bfs/archive/22f423fb530368393a6bb11f6fdeae676a6f0407.jpg)(title-人脸识别迎宾系统)]

## 视频或摄像头人脸识别功能
源码如下:com.itboyst.facedemo.OpencvTest
```
package com.itboyst.facedemo;

import cn.hutool.core.collection.CollectionUtil;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.Rect;
import com.arcsoft.face.toolkit.ImageInfo;
import com.google.common.collect.Lists;
import com.itboyst.facedemo.dto.FaceDetectResDTO;
import com.itboyst.facedemo.dto.ProcessInfo;
import com.itboyst.facedemo.dto.UserCompareInfo;
import com.itboyst.facedemo.service.FaceEngineService;
import com.itboyst.facedemo.service.UserFaceInfoService;
import com.itboyst.facedemo.util.ImageGUI;
import com.itboyst.facedemo.util.ImageUtil;
import com.itboyst.facedemo.util.ShowVideo;
import com.itboyst.facedemo.util.UserRamCache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

import static com.arcsoft.face.toolkit.ImageFactory.bufferedImage2ImageInfo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpencvTest {

    @Autowired
    UserFaceInfoService userFaceInfoService;
    @Autowired
    FaceEngineService faceEngineService;

    @Test
    public void testOpencv() {
        System.load("C:\\lib\\opencv_java320.dll");
        System.load("C:\\lib\\opencv_ffmpeg320_64.dll");
        System.load("C:\\lib\\opencv_world320.dll");

        // 打开摄像头或者视频文件
        // device为0默认打开笔记本电脑自带摄像头，若为0打不开外置usb摄像头
        // 请把device修改为1或2再重试，1，或2为usb插上电脑后，电脑认可的usb设备id
        VideoCapture capture = new VideoCapture();
        //capture.open(0);
        capture.open("C:\\vip1.mp4");
        if (!capture.isOpened()) {
            System.out.println("could not load video data...");
            return;
        }
        int frameWidth = (int) capture.get(3);
        //int frameWidth = 720;
        int frameHeight = (int) capture.get(4);
        //int frameHeight = 480;
        ImageGUI gui = new ImageGUI();
        gui.createWin("camera", new Dimension(frameWidth, frameHeight));
        Mat frame = new Mat();
        List<FaceDetectResDTO> faceDetectResDTOS = Lists.newLinkedList();
        int index = 0;
        while (true) {
            boolean have = capture.read(frame);
            // Win上摄像头
            Core.flip(frame, frame, 1);
            if (!have) {
                break;
            }
            if (!frame.empty()) {
                BufferedImage bufferedImage = ImageUtil.mat2BufImg(frame, ".jpg");
                if (bufferedImage != null) {
                    //视频转换为图片
                    long start = System.currentTimeMillis();
                    ImageInfo videoImageInfo = bufferedImage2ImageInfo(bufferedImage);
                    //特征视频图片提取
                    List<FaceInfo> videoFaceInfoList = faceEngineService.detectFaces(videoImageInfo);
                    List<ProcessInfo> process = faceEngineService.process(videoImageInfo, videoFaceInfoList);
                    if (videoFaceInfoList!=null && videoFaceInfoList.size() > 0) {
                        index++;
                        if (index >= 5) {
                            //特征比对
                            faceDetectResDTOS.clear();
                            for (int i = 0; i < videoFaceInfoList.size(); i++) {
                                FaceDetectResDTO faceDetectResDTO = new FaceDetectResDTO();
                                FaceInfo faceInfo = videoFaceInfoList.get(i);
                                faceDetectResDTO.setRect(faceInfo.getRect());
                                faceDetectResDTO.setFaceId(faceInfo.getFaceId());
                                faceDetectResDTO.setOrient(faceInfo.getOrient());
                                faceDetectResDTO.setWearGlasses(faceInfo.getWearGlasses());
                                faceDetectResDTO.setLeftEyeClosed(faceInfo.getLeftEyeClosed());
                                faceDetectResDTO.setRightEyeClosed(faceInfo.getRightEyeClosed());
                                faceDetectResDTO.setFaceShelter(faceInfo.getFaceShelter());
                                faceDetectResDTO.setFaceData(faceInfo.getFaceData());
                                if (CollectionUtil.isNotEmpty(process)) {
                                    ProcessInfo processInfo = process.get(i);
                                    faceDetectResDTO.setAge(processInfo.getAge());
                                    faceDetectResDTO.setGender(processInfo.getGender());
                                    faceDetectResDTO.setLiveness(processInfo.getLiveness());
                                }
                                byte[] feature = faceEngineService.extractFaceFeature(videoImageInfo, faceInfo);

                                if (feature != null) {
                                    List<UserCompareInfo> userCompareInfos = faceEngineService.faceRecognition(feature, UserRamCache.getUserList(), 0.8f);
                                    if (CollectionUtil.isNotEmpty(userCompareInfos)) {
                                        faceDetectResDTO.setName(userCompareInfos.get(0).getName());
                                        faceDetectResDTO.setSimilar(userCompareInfos.get(0).getSimilar());
                                        faceDetectResDTO.setFaceId(userCompareInfos.get(0).getFaceId());
                                    }
                                }
                                long end = System.currentTimeMillis();
                                System.out.println("检测耗时:" + (end - start) + "ms");
                                faceDetectResDTOS.add(faceDetectResDTO);


                            }
                            //System.out.println(index);
                            index = 0;
                        }
                        //标记人脸信息
                        for (FaceDetectResDTO faceDetectResDTO : faceDetectResDTOS) {
                            int faceId = faceDetectResDTO.getFaceId();
                            String name = faceDetectResDTO.getName();
                            float score = faceDetectResDTO.getSimilar();
                            Rect rect = faceDetectResDTO.getRect();
                            int age = faceDetectResDTO.getAge();
                            int gender = faceDetectResDTO.getGender();
                            int liveness = faceDetectResDTO.getLiveness();
                            String genderStr = gender == 0 ? "男" : "女";
                            String livenessStr = (liveness == 1 ? "活体" : "非活体");
                            //Imgproc.putText(frame, name + " " + String.valueOf(score), new Point(rect.left, rect.top), 0, 1.0, new Scalar(0, 255, 0), 1, Imgproc.LINE_AA, false);
                            frame = ImageUtil.addMark(bufferedImage, videoFaceInfoList, faceId, name, score, rect, age, genderStr, livenessStr);
                        }
                    }
                    gui.imshow(ShowVideo.conver2Image(frame));
                    gui.repaint();
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

```
**注意修改上面源码中的jar包与dll地址**
运行com.itboyst.facedemo.OpencvTest就可以加载视频或是打开代码中capture.open(0)的注释就可以实现摄像头实时识别了,以上就是源码的全部功能,进一步的需求还需要盆友们自己去探索,此处抛砖引玉.
源码地址: